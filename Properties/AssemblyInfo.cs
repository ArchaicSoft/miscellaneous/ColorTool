﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Color Manipulator Tool")]
[assembly: AssemblyDescription("For color replacement manipulation on one or more graphic files at once.")]
[assembly: AssemblyCompany("ArchaicSoft")]
[assembly: AssemblyProduct("Color Tool")]
[assembly: AssemblyCopyright("Copyright © 2016 Damien Gibson")]
[assembly: Guid("21abeb49-102b-4a3b-9301-1a51ddb90824")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]

