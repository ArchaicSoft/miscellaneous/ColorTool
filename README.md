This product is developed by ArchaicSoft as a free Open-Source application. It follows the BSD 3-Clause License Agreement.

If you have paid to receive this library you should immediately request your money back.

You may find this products source on https://gitlab.com/OfficialArchaicSoft/Cs-ColorTool

Questions or troubleshooting can be requested to:
ArchaicSoft@outlook.com